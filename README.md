# Kohana PHP Framework

[Kohana](http://kohanaframework.org/) is an elegant, open source, and object oriented HMVC framework built using PHP5, by a team of volunteers. It aims to be swift, secure, and small.

Released under a [BSD license](http://kohanaframework.org/license), Kohana can be used legally for any open source, commercial, or personal project.

## Documentation
Kohana's documentation can be found at <http://kohanaframework.org/documentation> which also contains an API browser.

The `userguide` module included in all Kohana releases also allows you to view the documentation locally. Once the `userguide` module is enabled in the bootstrap, it is accessible from your site via `/index.php/guide` (or just `/guide` if you are rewriting your URLs).

## Reporting bugs
If you've stumbled across a bug, please help us out by [reporting the bug](http://dev.kohanaframework.org/projects/kohana3/) you have found. Simply log in or register and submit a new issue, leaving as much information about the bug as possible, e.g.

* Steps to reproduce
* Expected result
* Actual result

This will help us to fix the bug as quickly as possible, and if you'd like to fix it yourself feel free to [fork us on GitHub](https://github.com/kohana) and submit a pull request!

Added RESTful API

GET method (json) (list data)
http://blog-kohana/blog/index.json

GET method (html) (list data)
http://blog-kohana/blog/index_html.html

POST method (create data)
http://blog-kohana/blog/create
Content-Type: multipart/form-data;
title - string
description - string
full_text - string
img - file
publication_date - datetime (2017-09-07 11:00:00)
publication_flag - string (on, NULL)

PUT method (update data)
http://blog-kohana/blog/update.json
Content-Type: application/x-www-form-urlencoded;
id - integer
title - string
publication_date - datetime (publication_date)