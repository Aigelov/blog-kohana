<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?= $title ?></title>
    <link rel="stylesheet" href="/public/css/bootstrap.min.css">
    <link rel="stylesheet" href="/public/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="/public/css/style.css">
</head>

<body>
<script src="/public/js/jquery-3.2.1.min.js" type="application/javascript"></script>
<script src="/public/js/moment-with-locales.min.js" type="application/javascript"></script>
<script src="/public/js/bootstrap.min.js" type="application/javascript"></script>
<script src="/public/js/bootstrap-datetimepicker.min.js" type="application/javascript"></script>
<?= $content; ?>
</body>
</html>