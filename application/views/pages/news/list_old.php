<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <h1>Список новостей</h1>
            <p><?= html::anchor('news/new', 'Добавить новость', ['class' => 'btn btn-primary']) ?></p><br />

            <?php if (count($news)): ?>
                <table class="table">
                    <thead>
                    <th>Статус публикации</th>
                    <th>Дата публикации</th>
                    <th>Название</th>
                    <th>Описание</th>
                    <th>Полный текст</th>
                    <th>Изображение</th>
                    <th>Дата создания</th>
                    <th>Дата изменения</th>
                    <th></th>
                    </thead>
                    <tbody>
                    <?php foreach($news as $item): ?>
                        <tr>
                            <td><?= $item['publication_flag'] == 1 ? 'Опубликован' : 'Не опубликован'; ?></td>
                            <td><?= $item['publication_date']; ?></td>
                            <td><?= $item['title']; ?></td>
                            <td><?= $item['description']; ?></td>
                            <td><?= Text::limit_chars($item['full_text'], 200); ?></td>
                            <td>
                                <?php if ($item['img']) : ?>
                                    <a target="_blank" href="<?= URL::site("/uploads/".$item['img']); ?>">
                                        <img src="<?= URL::site("/uploads/".$item['img']); ?>" alt="Uploaded image" class="img-responsive" width="100" />
                                    </a>
                                <?php endif; ?>
                            </td>
                            <td><?= $item['created_at']; ?></td>
                            <td><?= $item['updated_at']; ?></td>
                            <td>
                                <?= html::anchor('/news/show/'. $item['id'], 'Просмотр', ['class' => 'btn btn-info btn-sm']) ?>
                                <?= html::anchor('/news/edit/'. $item['id'], 'Редактировать', ['class' => 'btn btn-warning btn-sm']) ?>
                                <?= html::anchor('/news/delete/'. $item['id'], 'Удалить', ['class' => 'btn btn-danger btn-sm', 'id' => 'delete-item']) ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            <?php else: ?>
                <div>Нету новостей</div>
            <?php endif; ?>
        </div>
    </div>
</div>

<script>
    $('#delete-item').on('click', function(e) {
        if (!confirm('Вы действительно хотите удалить новость "<?= $item['title'] ?>"?')) {
            e.preventDefault();
        }
    });
</script>
