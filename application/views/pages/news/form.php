<?php defined('SYSPATH') or die('No direct script access.');
/**
 * @author Galymzhan Aigelov <aigelov@gmail.com>
 * @views views/pages/news
 * created on 10.09.2017
 */

/**
 * @var Model_News      $news               Blog news object
 * @var string          $title              Title type string
 * @var string          $description        Description type string
 * @var string          $full_text          Fulltext type string
 * @var string          $img                Image type string
 * @var datetime        $publication_date   Publication date type datetime
 * @var boolean         $publication_flag   Publication flag type boolean
 * @var array           $item               Current item data
 */
?>
<form action="<?= URL::site('/news/save') ?>" method="post" name="news-form" enctype="multipart/form-data">
    <div class="form-group">
        <label for="title">Название</label>
        <input type="text" class="form-control" id="title" name="title" placeholder="Название" value="<?= Arr::get($item, 'title') ?>" />
        <?php if (!empty($errors) && Arr::get($errors, 'title')) : ?>
            <div class="text-danger"><?= Arr::get($errors, 'title') ?></div>
        <?php endif; ?>
    </div>
    <div class="form-group">
        <label for="description">Описание</label>
        <input type="text" class="form-control" id="description" name="description" placeholder="Описание" value="<?= Arr::get($item, 'description') ?>" />
        <?php if (!empty($errors) && Arr::get($errors, 'description')) : ?>
            <div class="text-danger"><?= Arr::get($errors, 'description') ?></div>
        <?php endif; ?>
    </div>
    <div class="form-group">
        <label for="full_text">Текст</label>
        <textarea rows="3" class="form-control" id="full_text" name="full_text" placeholder="Текст"><?= Arr::get($item, 'full_text') ?></textarea>
        <?php if (!empty($errors) && Arr::get($errors, 'full_text')) : ?>
            <div class="text-danger"><?= Arr::get($errors, 'full_text') ?></div>
        <?php endif; ?>
    </div>
    <div class="form-group">
        <label for="img">Изображение</label>
        <input type="file" class="form-control" id="img" name="img" value="<?= Arr::get($item, 'img') ?>" />
        <?php if (!empty($errors) && Arr::get($errors, 'img')) : ?>
            <div class="text-danger"><?= Arr::get($errors, 'img') ?></div>
        <?php endif; ?>
    </div>
    <div class="form-group">
        <label for="publication_date">Дата публикации</label>
        <div class="input-group date">
            <input type="text" class="form-control" id="publication_date" name="publication_date" value="<?= Arr::get($item, 'publication_date') ?>" />
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
        </div>
        <?php if (!empty($errors) && Arr::get($errors, 'publication_date')) : ?>
            <div class="text-danger"><?= Arr::get($errors, 'publication_date') ?></div>
        <?php endif; ?>
    </div>
    <div class="checkbox">
        <label>
            <input type="checkbox" name="publication_flag" <?= Arr::get($item, 'publication_flag') == 1 ? 'checked' : '' ?>> Опубликовать
        </label>
    </div>

    <button type="submit" name="save" class="btn btn-success">Сохранить</button>
    <input type="hidden" name="id" value="<?= Arr::get($item, 'id') ?>">
</form>

<script type="text/javascript">
    $(function () {
        var $publication_date = $('#publication_date');
        $publication_date.datetimepicker({
            locale: 'ru'
        });
        $publication_date.val("<?= Arr::get($item, 'publication_date') ?>");
    });
</script>