<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * @author Galymzhan Aigelov <aigelov@gmail.com>
 * @views views/pages/news
 * created on 10.09.2017
 */

/**
 * @var Model_News      $news               Blog news object
 * @var array           $item               Current item data
 */
?>

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <h1><?= Arr::get($item, 'title') ?></h1>
            <h4><?= Arr::get($item, 'description') ?></h4>
        </div>
        <div class="col-xs-6">
            <?= Arr::get($item, 'full_text') ?>
        </div>
        <div class="col-xs-6 news-show-right">
            <p class="bg-info">
                <b>Статус публикации: </b>
                <?= Arr::get($item, 'publication_flag') == 1 ? 'Опубликован' : 'Не опубликован' ?>
            </p>
            <p class="bg-info">
                <b>Дата публикации: </b>
                <?= Arr::get($item, 'publication_date') ?>
            </p>
            <div><img src="<?= URL::site("/uploads/".$item['img']); ?>" alt="Uploaded image" class="img-responsive" /></div>
        </div>
        <div class="col-xs-12">
            <br />
            <div style="margin-bottom: 25px;"><?= html::anchor('/', 'Назад', ['class' => 'btn btn-default']) ?></div>
        </div>
    </div>
</div>