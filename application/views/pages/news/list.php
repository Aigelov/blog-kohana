<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <h1>Список новостей</h1>
            <p><a href="/news/new" class="btn btn-primary">Добавить новость</a> </p><br />

            {% if news|length > 0 %}
            <table class="table">
                <thead>
                <th>Статус публикации</th>
                <th>Дата публикации</th>
                <th>Название</th>
                <th>Описание</th>
                <th>Полный текст</th>
                <th>Изображение</th>
                <th>Дата создания</th>
                <th>Дата изменения</th>
                <th></th>
                </thead>
                <tbody>
                {% for item in news %}
                    <tr>
                        <td>{{ item.publication_flag == 1 ? 'Опубликован' : 'Не опубликован' }}</td>
                        <td>{{ item.publication_date }}</td>
                        <td>{{ item.title }}</td>
                        <td>{{ item.description }}</td>
                        <td>{{ item.full_text }}</td>
                        <td>
                            {% if item.img|length > 0  %}
                                <a target="_blank" href="/uploads/{{ item.img }}">
                                    <img src="/uploads/{{ item.img }}" alt="Uploaded image" class="img-responsive" width="100" />
                                </a>
                            {% endif %}
                        </td>
                        <td>{{ item.created_at }}</td>
                        <td>{{ item.updated_at }}</td>
                        <td>
                            <a href="/news/show/{{ item.id }}" class="btn btn-info btn-sm">Просмотр</a>
                            <a href="/news/edit/{{ item.id }}" class="btn btn-warning btn-sm">Редактировать</a>
                            <a href="/news/delete/{{ item.id }}" class="btn btn-danger btn-sm delete-item" data-title="{{item.title}}">Удалить</a>
                        </td>
                    </tr>
                {% endfor %}
                </tbody>
            </table>
            {% else %}
                <div>Нету новостей</div>
            {% endif %}
        </div>
    </div>
</div>

<script>
    $('.delete-item').on('click', function(e) {
        var title = $(this).data('title');
        if (!confirm('Вы действительно хотите удалить новость "'+ title +'"?')) {
            e.preventDefault();
        }
    });
</script>
