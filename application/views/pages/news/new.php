<?php defined('SYSPATH') OR die('No direct access allowed.');
/**
 * @author Galymzhan Aigelov <aigelov@gmail.com>
 * @views views/pages/news
 * created on 10.09.2017
 */

/**
 * @var Model_News      $news               Blog news object
 * @var array           $item               Current item data
 */
?>

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <h1>Добавление новости</h1>
            <div style="margin-bottom: 25px;"><?= html::anchor('/', 'Назад', ['class' => 'btn btn-default']) ?></div>

            <?= View::factory('pages/news/form', ['item' => $item]) ?>
        </div>
    </div>
</div>