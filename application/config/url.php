<?php defined('SYSPATH') or die('No direct script access.');
/**
 * @author Galymzhan Aigelov <aigelov@gmail.com>
 * @config application/config/
 * created on 10.09.2017
 */

return array(
    'trusted_hosts' => array(
        'blog-kohana',
    ),
);