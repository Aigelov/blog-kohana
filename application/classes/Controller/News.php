<?php defined('SYSPATH') or die('No direct script access.');

class Controller_News extends Controller_Common
{
	public function action_index()
	{
        $news_model = new Model_News();
        $news = $news_model->get_all();

//        $content = View::factory('/pages/news/list', ['news' => $news]);
        $content = Twig::factory('/pages/news/list', ['news' => $news]);

		$this->template->content = $content;
	}


    public function action_show()
    {
        $news_id = $this->request->param('id');

        if (!$news_id)
        {
            throw new HTTP_Exception_404('News not found.');
        }

        // Get news
        $news = ORM::factory('news', $news_id);

        if (!$news->loaded())
        {
            throw new HTTP_Exception_404('News not found.');
        }

        // Set content template
        $this->template->set(
            'content',
            View::factory('pages/news/show', ['item' => $news->as_array()])
        );
    }


    public function action_new()
    {
        $news = ORM::factory('news')->as_array();

        // Set content template
        $this->template->set(
            'content',
            View::factory('pages/news/new', ['item' => $news])
        );
    }


    public function action_edit()
    {
        $news_id = $this->request->param('id');

        if (!$news_id)
        {
            throw new HTTP_Exception_404('News not found.');
        }

        // Get news
        $news = ORM::factory('news', $news_id);

        if (!$news->loaded())
        {
            throw new HTTP_Exception_404('News not found.');
        }

        // Set content template
        $this->template->set(
            'content',
            View::factory('pages/news/edit', ['item' => $news->as_array()])
        );
    }


    public function action_delete()
    {
        $news_id = $this->request->param('id');

        if (!$news_id)
        {
            throw new HTTP_Exception_404('News not found.');
        }

        // Get news
        $news = ORM::factory('news', $news_id);

        if (!$news->loaded())
        {
            throw new HTTP_Exception_404('News not found.');
        }

        $news->delete();

        // redirect to list page
        $this->redirect('/');
    }


    /**
     * Save news
     *
     * @throws HTTP_Exception_404
     */
    public function action_save()
    {
        // Protect page
        if ($this->request->method() !== Request::POST)
        {
            throw new HTTP_Exception_404('Page not found.');
        }

        // create and configure form validation
        $post = Validation::factory($this->request->post())
            ->rule('title', 'not_empty')
            ->rule('publication_date', 'not_empty');

        // check validation
        if ($post->check())
        {
            // store
            $data = $post->data();

            /** @var Model_News $news **/
            $news = ORM::factory('news', Arr::get($data, 'id'));

            if (Arr::get($data, 'publication_flag') == 'on')
                $data['publication_flag'] = 1;
            else
                $data['publication_flag'] = 0;

            if (Arr::get($data, 'publication_date'))
            {
                $data['publication_date'] = Date::formatted_time(Arr::get($data, 'publication_date'), 'Y-m-d H:i:s');
            }

            if (!Arr::get($post->data(), 'id'))
                $data['created_at'] = Date::formatted_time(date("Y-m-d H:i:s"));

            if (isset($_FILES['img']) && !empty($_FILES['img']['tmp_name']))
            {
                $data['img'] = $this->_save_image($_FILES['img']);
            }

            // update user
            $news->values($data, ['title', 'description', 'full_text', 'img', 'publication_date', 'publication_flag', 'created_at'])->save();

            // redirect to list page
            $this->redirect('/');
        }

        // Errors list
        View::set_global('errors', $post->errors('validation'));

        // Set content template
        $this->template->set('content', View::factory('/pages/news/' . (Arr::get($post->data(), 'id') ? 'edit' : 'new'),
            array(
                'item' => $post->data()
            )
        ));
    }


    protected function _save_image($image)
    {
        if (
            ! Upload::valid($image) OR
            ! Upload::not_empty($image) OR
            ! Upload::type($image, array('jpg', 'jpeg', 'png', 'gif')))
        {
            return FALSE;
        }

        $directory = DOCROOT.'uploads/';

        if ($file = Upload::save($image, NULL, $directory))
        {
            $filename = strtolower(Text::random('image', 10)).'.jpg';

            Image::factory($file)
                ->resize(200, 200, Image::AUTO)
                ->save($directory.$filename);

            // Delete the temporary file
            unlink($file);

            return $filename;
        }

        return FALSE;
    }

} // End News