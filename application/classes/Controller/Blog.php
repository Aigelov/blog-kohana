<?php defined('SYSPATH') or die('No direct script access.');
/**
 * @author Galymzhan Aigelov <aigelov@gmail.com>
 * @controller application/classes/Controller/
 * created on 11.09.2017
 */

/**
 * An example controller that implements a RESTful API.
 *
 * @TODO Move all default action functions into the REST parent class.
 *
 * @package  RESTfulAPI
 * @category Controller
 */
class Controller_Blog extends Controller_Rest
{
    /**
     * A Restexample model instance for all the business logic.
     *
     * @var Model_Blog
     */
    protected $_rest;

    /**
     * Initialize the example model.
     */
    public function before()
    {
        parent::before();
        $this->_rest = Model_RestAPI::factory('Blog');
    }

    /**
     * Handle GET requests.
     */
    public function action_index()
    {
        try
        {
            if ($this->request->method() == "GET")
                $this->rest_output($this->_rest->get($this->_params));
        }
        catch (Kohana_HTTP_Exception $khe)
        {
            $this->_error($khe);
            return;
        }
        catch (Kohana_Exception $e)
        {
            $this->_error('An internal error has occurred', 500);
            throw $e;
        }
    }

    /**
     * Handle GET requests.
     */
    public function action_index_html()
    {
        try
        {
            if ($this->request->method() == "GET")
                $this->rest_output($this->_rest->get($this->_params));
        }
        catch (View_Exception $e)
        {
            // Fall back to an empty string.
            // This way we don't have to satisfy *all* API requests as HTML.
            return '';
        }
    }

    /**
     * Handle POST requests.
     */
    public function action_create()
    {
        try
        {
            if ($this->request->method() == "POST")
                $this->rest_output($this->_rest->create($this->_params));
        }
        catch (Kohana_HTTP_Exception $khe)
        {
            $this->_error($khe);
            return;
        }
        catch (Kohana_Exception $e)
        {
            $this->_error('An internal error has occurred', 500);
            throw $e;
        }
    }

    /**
     * Handle PUT requests.
     */
    public function action_update()
    {
        try
        {
            $this->rest_output($this->_rest->update($this->_params));
        }
        catch (Kohana_HTTP_Exception $khe)
        {
            $this->_error($khe);
            return;
        }
        catch (Kohana_Exception $e)
        {
            $this->_error('An internal error has occurred', 500);
            throw $e;
        }
    }
}