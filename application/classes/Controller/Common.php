<?php defined('SYSPATH') or die('No direct script access.');
/**
 * @author Galymzhan Aigelov <aigelov@gmail.com>
 * @controller application/Controller/
 * created on 10.09.2017
 */

abstract class Controller_Common extends Controller_Template
{
    public $template = 'main';

    public function before()
    {
        parent::before();
        View::set_global('title', 'Блог на кохане');
        $this->template->content = '';
        $this->template->styles = '';
        $this->template->scripts = '';
    }

} // End Common