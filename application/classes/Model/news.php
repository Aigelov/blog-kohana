<?php defined('SYSPATH') or die('No direct script access.');

/**
 * @author Galymzhan Aigelov <aigelov@gmail.com>
 * @model application/classes/Model/
 * created on 10.09.2017
 */

class Model_News extends ORM
{
    protected $_tableNews = 'news';

    protected $_primary_key = 'id';

    public function validate(array & $array, $save = FALSE)
    {
        $array = Validation::factory($array)
            ->add_rules('title', 'required', 'length[5, 255]')
            ->add_rules('description','is_string')
            ->add_rules('full_text', 'length[15, 1000]')
            ->add_rules('publication_date', 'required');

        return parent::validate($array, $save);
    }

    /**
     * Get all news
     * @return array
     */
    public function get_all()
    {
        $sql = "SELECT * FROM ". $this->_tableNews ." ORDER BY publication_date DESC";

        return DB::query(Database::SELECT, $sql)
            ->execute();
    }
}