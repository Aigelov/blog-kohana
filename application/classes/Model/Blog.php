<?php defined('SYSPATH') or die('No direct access allowed.');
/**
 * @author Galymzhan Aigelov <aigelov@gmail.com>
 * @model application/classes/Controller/
 * created on 11.09.2017
 */

/**
 * An example REST model
 *
 * @package  RESTfulAPI
 * @category Model
 */
class Model_Blog extends Model_RestAPI
{
    protected $_tableNews = 'news';
    protected $_primary_key = 'id';

    /**
     * Get all news
     *
     * @param $params
     * @return array
     */
    public function get($params)
    {
        $result = DB::query(Database::SELECT, "SELECT * FROM ". $this->_tableNews ." ORDER BY publication_date DESC")
            ->execute();
        $result_arr = [];

        foreach($result as $key => $item)
        {
            $result_arr[$key] = $item;
        }

        return $result_arr;
    }

    /**
     * Create news
     *
     * @param $params
     * @return array
     * @throws HTTP_Exception
     */
    public function create($params)
    {
        // Enforce and validate some parameters.
        if (!isset($params['title']))
        {
            throw HTTP_Exception::factory(400, array(
                'error' => __('Missing title'),
                'field' => 'title',
            ));
        }

        if (!Valid::min_length($params['title'], 5))
        {
            throw HTTP_Exception::factory(400, array(
                'error' => __('The title must contain at least 2 characters'),
                'field' => 'title',
            ));
        }

        // Process the request and create a new object.
        // create and configure form validation
        $post = Validation::factory($params)
            ->rule('title', 'not_empty')
            ->rule('publication_date', 'not_empty');

        // check validation
        if ($post->check())
        {
            // store
            $data = $post->data();

            /** @var Model_News $news **/
            $news = ORM::factory('news', Arr::get($data, 'id'));

            if (Arr::get($data, 'publication_flag') == 'on')
                $data['publication_flag'] = 1;
            else
                $data['publication_flag'] = 0;

            if (Arr::get($data, 'publication_date'))
            {
                $data['publication_date'] = Date::formatted_time(Arr::get($data, 'publication_date'), 'Y-m-d H:i:s');
            }

            if (!Arr::get($post->data(), 'id'))
                $data['created_at'] = Date::formatted_time(date("Y-m-d H:i:s"));

            if (isset($_FILES['img']) && !empty($_FILES['img']['tmp_name']))
            {
                $data['img'] = $this->_save_image($_FILES['img']);
            }

            // update user
            $news->values($data, ['title', 'description', 'full_text', 'img', 'publication_date', 'publication_flag', 'created_at'])->save();

            return $post->data();
        }

        return $post->errors('validation');
    }

    /**
     * Update news
     *
     * @param $params
     * @return array
     * @throws HTTP_Exception
     */
    public function update($params)
    {
        $news_id = $params['id'];

        if (!$news_id)
        {
            throw HTTP_Exception::factory(400, array(
                'error' => __('News not found.')
            ));
        }

        // Enforce and validate some parameters.
        if (!isset($params['title']))
        {
            throw HTTP_Exception::factory(400, array(
                'error' => __('Missing title'),
                'field' => 'title',
            ));
        }

        if (!Valid::min_length($params['title'], 5))
        {
            throw HTTP_Exception::factory(400, array(
                'error' => __('The title must contain at least 2 characters'),
                'field' => 'title',
            ));
        }

        // Process the request and create a new object.
        // create and configure form validation
        $post = Validation::factory($params)
            ->rule('title', 'not_empty')
            ->rule('publication_date', 'not_empty');

        // check validation
        if ($post->check())
        {
            // store
            $data = $post->data();

            /** @var Model_News $news **/
            $news = ORM::factory('news', Arr::get($data, 'id'));

            if (Arr::get($data, 'publication_flag') == 'on')
                $data['publication_flag'] = 1;
            else
                $data['publication_flag'] = 0;

            if (Arr::get($data, 'publication_date'))
            {
                $data['publication_date'] = Date::formatted_time(Arr::get($data, 'publication_date'), 'Y-m-d H:i:s');
            }

            if (isset($_FILES['img']) && !empty($_FILES['img']['tmp_name']))
            {
                $data['img'] = $this->_save_image($_FILES['img']);
            }

            // update user
            $news->values($data, ['title', 'description', 'full_text', 'img', 'publication_date', 'publication_flag', 'created_at'])->save();

            return $post->data();
        }

        return $post->errors('validation');
    }

    protected function _save_image($image)
    {
        if (
            ! Upload::valid($image) OR
            ! Upload::not_empty($image) OR
            ! Upload::type($image, array('jpg', 'jpeg', 'png', 'gif')))
        {
            return FALSE;
        }

        $directory = DOCROOT.'uploads/';

        if ($file = Upload::save($image, NULL, $directory))
        {
            $filename = strtolower(Text::random('image', 10)).'.jpg';

            Image::factory($file)
                ->resize(200, 200, Image::AUTO)
                ->save($directory.$filename);

            // Delete the temporary file
            unlink($file);

            return $filename;
        }

        return FALSE;
    }
}